const fs = require('fs')

async function createAndDeleteRandomJOSNFiles(directoryName, randomFilesCount) {
    try {
        await createDirectory()
        await createRandomJSONFiles()
        await deleteCreatedRandomJSONFiles()
    } catch {
        console.error(err)
    }

    function createDirectory() {
        return new Promise((resolve, reject) => {
            fs.mkdir(directoryName, (err) => {
                if (err) {
                    console.log(err)
                    reject(err)
                } else {
                    resolve(`${directoryName} Directory created successfully`)
                }
            })
        })
    }

    function createRandomJSONFiles() {
        if (randomFilesCount < 1){
            randomFilesCount = 3
        }
        return new Promise((resolve, reject) => {
            for (let i = 0; i < randomFilesCount; i++) {
                fs.writeFile(`${directoryName}/randomJOSNFile${i + 1}.txt`, 'lipsumData', (err) => {
                    if (err) {
                        reject(err)
                    } else {
                        resolve(`${directoryName}/randomJOSNFile${i + 1}.txt JSONFile created successfully`)
                    }
                })
            }
        })
    }

    function deleteCreatedRandomJSONFiles() {
        return new Promise((resolve, reject) => {
            for (let i = 0; i < randomFilesCount; i++) {
                fs.unlink(`${directoryName}/randomJOSNFile${i + 1}.txt`, (err) => {
                    if (err) {
                        reject(err)
                    } else {
                        resolve(`${directoryName}/randomJOSNFile${i + 1}.txt JSONFile deleted successfully`)
                    }
                })
            }
        })
    }
}
module.exports = createAndDeleteRandomJOSNFiles
