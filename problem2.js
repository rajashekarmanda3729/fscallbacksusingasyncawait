const fs = require('fs')
const { resolve } = require('path')

async function performActionsOnFiles(filePath) {
    try{
        let dataLipsum = await readAFile()
        await convertToUpperCase(dataLipsum)
        await createFileaNamesFile(dataLipsum)
        await convertToLowerCase(dataLipsum)
        await sortLisumData(dataLipsum)
        let fileNamesList = await readFileNamesFile()
        await deleteFileNamesFiles(fileNamesList)
    }catch(err){
        console.log(err)
    }

    function readAFile() {
        return new Promise((resolve, reject) => {
            fs.readFile('lipsum.txt', 'utf-8', (err, data) => {
                if (err) {
                    reject(err)
                } else {
                    resolve('readed lipsum file')
                }
            })
        })
    }

    function convertToUpperCase(data) {
        const dataUpperCase = data.toUpperCase()
        return new Promise((resolve, reject) => {
            fs.writeFile(`${filePath}/lipsumUpperCase.txt`, dataUpperCase, (err, data) => {
                if (err) {
                    reject(err)
                } else {
                    resolve('converted to uppercase ')
                }
            })
        })
    }
    function createFileaNamesFile() {
        return new Promise((resolve, reject) => {
            fs.writeFile(`${filePath}/fileNames.txt`, `lipsumUpperCase.txt`, (err, data) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(' writed in fileNames.txt ')
                }
            })
        })
    }
    function convertToLowerCase(data) {
        return new Promise((resolve, reject) => {
            fs.writeFile(`${filePath}/lowerCaseLipsum.txt`, data.toLowerCase().split('\n'), (err) => {
                if (err) {
                    reject(err)
                } else {
                    resolve('converted to lowercase')
                }
            })
            fs.appendFile(`${filePath}/fileNames.txt`, '\nlowerCaseLipsum.txt', (err) => {
                if (err) {
                    reject(err)
                } else {
                    resolve('added text')
                }
            })
        })
    }

    function sortLisumData(data) {
        return new Promise((resolve, reject) => {
            fs.writeFile(`${filePath}/sortLipsum.txt`, data.split('.').sort(), (err) => {
                if (err) {
                    reject(err)
                } else {
                    resolve('sorted file successfully')
                }
            })
            fs.appendFile(`${filePath}/fileNames.txt`, '\nsortLipsum.txt', (err) => {
                if (err) {
                    reject(err)
                } else {
                    resolve('added text to fileNames.txt')
                }
            })
        })
    }

    function readFileNamesFile() {
        return new Promise((resolve, reject) => {
            fs.readFile(`${filePath}/fileNames.txt`, 'utf-8', (err, data) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(data)
                }
            })
        })
    }
    function deleteFileNamesFiles(data) {
        let fileNamesAll = data.split('\n')
        return new Promise((resolve, reject) => {
            for (let eachFile of fileNamesAll) {
                fs.unlink(`storeData/${eachFile}`,(err) => {
                    if (err) {
                        reject(err)
                    }else {
                        resolve('delered file '+ eachFile)
                    }
                })
            }
        })

    }
}
module.exports = performActionsOnFiles
